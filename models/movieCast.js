'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movieCast extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      movieCast.belongsTo(models.movie, {
        foreignKey: "movie_id", targetKey: "id"
      });

      movieCast.belongsTo(models.cast, {
        foreignKey: "cast_id", sourceKey: "id"
      })
    }
  };
  movieCast.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    movie_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    cast_id: {
      type: DataTypes.BIGINT,
      allowNull: false,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'movieCast',
    tableName:'movieCasts',
    timestamps: true,
  });
  return movieCast;
};