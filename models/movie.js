'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class movie extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      movie.hasMany(models.movieCast, {
        foreignKey: "movie_id", sourceKey: "id"
      });
    }
  };
  movie.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    name: {
      type: DataTypes.STRING(100)
    },
    language: {
      type: DataTypes.STRING(30)
    },
    status: {
      type: DataTypes.STRING(10)
    },
    rating: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'movie',
    timestamps:true,
    tableName: 'movies',
  });
  return movie;
};