'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cast extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      cast.belongsToMany(models.movie, {
        through: models.movieCast,
        foreignKey: "cast_id",
        targetKey: "id"
      })
    }
  };
  cast.init({
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.BIGINT
    },
    name: {
      type: DataTypes.STRING(100)
    },
    birthday: {
      type: DataTypes.DATE
    },
    deadday: {
      type: DataTypes.DATE,
      defaultValue: null
    },
    rating: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {
    sequelize,
    modelName: 'cast',
    tableName: 'casts',
    timestamps: true,
  });
  return cast;
};