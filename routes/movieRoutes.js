const router = require("express").Router();
const movieController = require("../controllers/movieController");

router.post("/movie/create", movieController.create);
router.get("/movie/showAllMovie", movieController.getMovie);
router.get("/movie/find/:movieId", movieController.getByIdMovie);
router.put("/movie/update/:movieId", movieController.update);
router.delete("/movie/delete/:movieId", movieController.delete);

module.exports = router;