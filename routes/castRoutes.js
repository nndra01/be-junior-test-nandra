const router = require("express").Router();
const castController = require("../controllers/castController");

router.post("/cast/create", castController.create);
router.get("/cast/showAllCast", castController.getAllCast);
router.get("/cast/find/:castId", castController.getIdCast);
router.put("/cast/update/:castId", castController.update);
router.delete("/cast/delete/:castId", castController.delete);

module.exports = router;