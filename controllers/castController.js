const { cast } = require("../models");
const castController = {}

//!Create new cast
castController.create = async (req, res) => {
  try {
    const { rating } = req.body;
    if (rating <= 5) {
      const createCast = await cast.create({
        ...req.body,
      });
      const castRes = {
        statusCode: 200,
        statusText: "success",
        message: "Cast has been create",
        data: createCast,
        };
        res.json(castRes);
    }
    else {
      const alert = {
        statusText: "failed",
        message: "rate cannot be greater than 5"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Show all cast
castController.getAllCast = async (req, res) => {
  try {
    const showCast = await cast.findAll();
    const castRes = {
      statusCode: 200,
      message: "Show all cast",
      data: showCast,
    };
    res.json(castRes);
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Show cast by castId
castController.getIdCast = async (req, res) => {
  try {
    const castId = req.params.castId;
    const findCast = await cast.findOne({
      where: { id: castId }
    });
    if (findCast) {
      res.send({
        statusCode: 200,
        statusText: "success",
        message: "show cast by id",
        data: findCast
      });
    }
    else {
      res.send({
        statusCode: 404,
        statusText: "failed",
        message: "id cast not found"
      })
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Edit cast
castController.update = async (req, res) => {
  try {
    const castId  = req.params.castId;
    const { rating } = req.body;
    const findCast = await cast.findOne({
      where: { id: castId },
    });
    if (findCast) {
      if (rating <= 5) {
        let updateCast = await cast.update(req.body,{
          where: { id: castId },
        });
        res.send({
          statusCode: 200,
          statusText: "success",
          message: "cast has been updated",
          data: updateCast
        });
      }
      else {
        const alert = {
          statusText: "failed",
          message: "rate cannot be greater than 5"
        }
        res.json(alert);
      }
    }
    else {
      const alert = {
        statusCode: 404,
        statusText: "failed",
        message: "id not found"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Delete cast
castController.delete = async (req, res) => {
  try {
    const castId = req.params.castId;
    const deleteCast = await cast.destroy({
      where: { id: castId }
    });
    if (deleteCast) {
      res.send({
        statusCode: 200,
        statusText: "success",
        message: "cast has been deleted"
      });
    }
    else {
      res.send({
        statusCode:404,
        statusText: "failed",
        message: "id cast not found"
      });
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};
module.exports = castController;