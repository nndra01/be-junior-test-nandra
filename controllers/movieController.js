const { movie, movieCast, cast } = require("../models");
const movieController = {};

//! Create new movie
movieController.create = async (req, res) => {
  try {
    const { rating } = req.body;
    if (rating <= 5) {
      const createMovie = await movie.create({
        ...req.body,
      });
      if(createMovie) {
        let cast_id = req.body.cast_id;
        cast_id = cast_id.trim();
        cast_id = cast_id.split(",");
        for(let i = 0; i < cast_id.length; i++) {
          await movieCast.create({
            movie_id: createMovie.dataValues.id,
            cast_id: cast_id[i],
          });
        }
      }
      const movieRes = {
        statusCode: 200,
        statusText: "success",
        message: "Movie has been create",
        data: createMovie,
        };
        res.json(movieRes);
    }
    else {
      const alert = {
        statusText: "failed",
        message: "rate cannot be greater than 5"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Show all movie
movieController.getMovie = async (req,res) =>{
  try {
    const showMovie = await movie.findAll({
      include: [{ model: movieCast, attributes: ["id","movie_id","cast_id"], include: [cast]}],
    });
    const movieRes = {
      statusCode: 200,
      message: "Show all movie",
      data: showMovie,
    };
    res.json(movieRes);
  } catch(error) {
    res.status(500).json({
     statusText: "Internal server error",
     message: error.message,
    });
  }
};

//!Show movie by MoviId
movieController.getByIdMovie = async (req, res) => {
  try {
    const movieId = req.params.movieId;
      const findMovie = await movie.findOne({
        where: { id: movieId },
        include: [{ model: movieCast, attributes: ["id","movie_id","cast_id"], include: [cast]}],
      });
      if (movieId) {
      res.send({
        statusCode: 200,
        statusText: "success",
        message: "show movie by id",
        data: findMovie
      });
    }
    else {
      const alert = {
        statusText: "failed",
        message: "id movie not found"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
     });
  }
};

//!Edit Movie
movieController.update = async (req, res) => {
  try {
    const movieId  = req.params.movieId;
    const { rating } = req.body;
    const findMovie = await movie.findOne({
      where: { id: movieId },
    });
    if (findMovie) {
      if (rating <= 5) {
        let updateMovie = await movie.update(req.body,{
          where: { id: movieId },
        });
        const delMovieCast = await movieCast.destroy({
          where: { id: movieId },
        });
        if(delMovieCast) {
          let cast_id = req.body.cast_id;
          cast_id = cast_id.trim();
          cast_id = cast_id.split(",");
          for(let i = 0; i < cast_id.length; i++) {
            await movieCast.create({
              movie_id: delMovieCast.dataValues.id,
              cast_id: cast_id[i],
            });
          }
        }
        res.send({
          statusCode: 200,
          statusText: "success",
          message: "Movie has been updated",
          data: updateMovie,
        });
      }
      else {
        const alert = {
          statusText: "failed",
          message: "rate cannot be greater than 5"
        }
        res.json(alert);
      }
    }
    else {
      const alert = {
        statusCode: 404,
        statusText: "failed",
        message: "id not found"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
    });
  }
};

//!Delete Movie
movieController.delete = async (req, res) => {
  try {
    const movieId = req.params.movieId;
    const findMovie = await movie.findOne({
      where: { id: movieId},
    });
    if (findMovie) {
      const deleteMovie = await movie.destroy({
        where: {
          id: movieId,
        },
      });
      res.send({
        statusCode: 200,
        message: "delete movie successful",
        data: deleteMovie
      })
    }
    else {
      const alert = {
        statusCode: 404,
        statusText: "failed",
        message: "id not found"
      }
      res.json(alert);
    }
  } catch(error) {
    res.status(500).json({
      statusText: "Internal server error",
      message: error.message,
     });
  }
};
module.exports = movieController;