const express = require("express");
const dotenv = require("dotenv");
const dotenvConfig = dotenv.config();

const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

const movieRoutes = require("./routes/movieRoutes");
const castRoutes = require("./routes/castRoutes");

app.use(
  "/api",
  movieRoutes,
  castRoutes,
);

app.get("/", (req, res) => {
  res.send({
    statusCode: 200,
    statusMessage: "Success",
    message: "Welcome to movie API",
  });
});

app.all("*", (req, res) => 
  res.status(404).json({
     statusText: "Not Found",
     message: "Route doesn't exist",
  })
);

app.listen(process.env.PORT || 5000, () => {
  console.log(`SERVER IS RUNNING ON PORT 5000 || ENV PORT : ${process.env.PORT}`);
});

module.exports = app;